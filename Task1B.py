# Copyright (C) 2018 Lab group 17
#
# SPDX-License-Identifier: MIT

from floodsystem.stationdata import build_station_list
from floodsystem.geo import order_by_dist


def run():
    """Requirements for Task 1B"""
    distances = order_by_dist(build_station_list(),(52.2053, 0.1218))
    
    closest_10=[]
    #finding the closest 10 stations
    for no_town_tuple in distances[:10]:
        #finding the town of the station
        for station in build_station_list():
            if no_town_tuple[0]==station.name:
                #create a tuple including the town
                with_town_tuple=(station.name, station.town, no_town_tuple[1])
                closest_10.append(with_town_tuple)
                continue

    """similar method as above"""
    farthest_10=[]
    for no_town_tuple in distances[-10:]:
        for station in build_station_list():
            if no_town_tuple[0]==station.name:
                with_town_tuple=(station.name, station.town, no_town_tuple[1])
                farthest_10.append(with_town_tuple)
                continue
    print(closest_10)
    print(farthest_10)


if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()
