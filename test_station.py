# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the station module"""

from floodsystem.station import MonitoringStation,inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town


"""
Checks that corrupt river range data can be found
Also tests that the corrupt stations can be extracted
Uses 3 made up stations
"""
def test_range_validation():
    s_id = "test-s-id"
    m_id = "test-m-id"
    coord = (-2.0, 4.0)
    river = "River X"
    town = "My Town"

    label = "s1"
    trange = (10, -1)
    s1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    assert s1.typical_range_consistent() == False

    label = "s2"
    trange = None
    s2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    assert s2.typical_range_consistent() == False

    label = "s3"
    trange = (1,2)
    s3 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)
    assert s3.typical_range_consistent() == True

    assert inconsistent_typical_range_stations([s1,s2,s3]) == ["s1","s2"]

#test using real date
def test_inconsistent_typical_range_stations():
    corrupt=inconsistent_typical_range_stations((build_station_list()))
    for station in corrupt:
        #the data of the station may no longer be invalid
        if station=="Airmyn":
            station_air=station
            break
    assert station_air

    for station in build_station_list():
        if station.name==corrupt[1]:
            range_1=station.typical_range
    assert range_1 is None or range_1[1]<range_1[0]