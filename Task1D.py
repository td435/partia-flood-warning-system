# Copyright (C) 2018 Lab group 17
#
# SPDX-License-Identifier: MIT

from floodsystem.stationdata import build_station_list

from floodsystem.geo import rivers_with_stations, stations_by_river


def run():
    """Requirements for Task 1D"""
    river_list = rivers_with_stations(build_station_list()) 
    river_index= stations_by_river(build_station_list())
    print("Number of unique rivers monitored: {}\n".format(len(river_list)))
    
    print("First 10 rivers with monitoring stations(s) sorted by alphabetical order: ")
    print(sorted(river_list)[:10])
    #print(river_index)

    print("Stations on River Aire")
    print(sorted(river_index["River Aire"]))
    print("Stations on River Cam")
    print(sorted(river_index["River Cam"]))
    print("Stations on River Thames")
    print(sorted(river_index["River Thames"]))

    
    


if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()
