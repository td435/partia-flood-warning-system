"""
Module for analysing exciting flud stuff
"""
import matplotlib as plt
import numpy as np


def polyfit(dates, levels, p):
    x = plt.dates.date2num(dates)
    y = levels
    dtheta = x[0]

    # Find coefficients of best-fit polynomial f(x) of degree 4
    p_coeff = np.polyfit(x- x[0], y, p)

    # Convert coefficient into a polynomial that can be evaluated,
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)

    return poly, dtheta,x
    