from .utils import sorted_by_key

def stations_level_over_threshold(stations, tol):
    level_over_tol=[]
    for station in stations:
        #select stations which has water level above the tolerance
        if station.relative_water_level() is not None and station.relative_water_level()>tol:
            level_over_tol.append((station.name, station.relative_water_level()))
    #sort the list by relative water level
    return sorted_by_key(level_over_tol, 1, reverse=True)

def stations_highest_rel_level(stations, N):
    highest_level=[]
    for station in stations:
        if station.relative_water_level() is not None:
            #highest_level.append((station.name, station.relative_water_level()))
            highest_level.append((station,station.relative_water_level()))
    #return N stations with the highest relative water level
    a=sorted_by_key(highest_level, 1, reverse=True)[:N]
    #return a list of stations
    stations_list=[]
    for item in a:
        stations_list.append(item[0])
    return stations_list