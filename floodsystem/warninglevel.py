import matplotlib
import numpy as np

from datetime import datetime, timedelta
from .plot import *
from .analysis import *

#current time is x[0]
def warning_level(station):
    if station.relative_water_level() is None:
        return "invalid data"

    dt = 2
    dates, levels = fetch_measure_levels(station.measure_id, dt=timedelta(days=dt))
    dx=1/96 #numerical representation of 15 minutes

    #some data cannot be retrieved, not sure if it has anything to do with my computer
    if len(dates)==0 or len (levels)==0 or len(dates)!=len(levels):
        return  "invalid data"

    poly, x0, x = polyfit(dates, levels, 4)
    gradient=np.polyder(poly)
    current=levels[0]
    

    #stations with very high relative water levels are in danger
    if station.relative_water_level()>=5:
        return "severe"

    #stations with high relative water levels and with increasing water levels 
    # are in severe risk, else the risk level is high
    elif station.relative_water_level()>=3:   
        #checking whether the relative level will increase by 1.1 times in the next 15 minutes
        if 0.0001*current/dx<gradient(0):
                return "severe"
        elif -0.0002*current/dx>gradient(0):
                return "moderate"
        else:
            return "high"
    
    #similar to the above one except that the moderate level is introduced
    elif station.relative_water_level()>=2:
        if 0.0001*current/dx<gradient(0):
            return "high"
        else:
            return "moderate"


    elif station.relative_water_level()>=0.8:
        if 0.00015*current/dx<gradient(0):
            return "moderate"
        else:
            return "low"
    
    else:
        return "low"

#inefficient
def warning_level1(station):
    if station.relative_water_level() is None:
        return "invalid data"

    dt = 2
    dates, levels = fetch_measure_levels(station.measure_id, dt=timedelta(days=dt))

    #some data cannot be retrieved, not sure if it has anything to do with my computer
    if len(dates)==0 or len (levels)==0 or len(dates)!=len(levels):
        return  "invalid data"

    poly, dtheta,x = polyfit(dates, levels, 4)
    dx=1/96 #numerical representation of 15 minutes

    #stations with very high relative water levels are in danger
    if station.relative_water_level()>=5:
        return "severe"

    #stations with high relative water levels and with increasing predicted water levels 
    # are in severe risk, else the risk level is high
    elif station.relative_water_level()>=3:   
        #checking whether the predicted level in the next 3 hours will be 1.5 times higher than the current level
        for i in range(int(len(x)/16)):
            if 1.5*levels[-1]<poly(-dx*i):
                return "severe"
        else:
            return "high"
    
    #similar to the above one except that the moderate level is introduced
    elif station.relative_water_level()>=1:
        for i in range(int(len(x)/16)):
            if 2.0*levels[-1]<poly(-dx*i):
                return "severe"
            elif 0.98*levels[-1]>poly(-dx*i):
                return "moderate"
        else:
            return "high"


    #low relative water levels associate with relative low risks
    else:
        for i in range(int(len(x)/16)):
            if 1.25*station.typical_range[1]<poly(d-x*i):
                return "moderate"
        else:
            return "low"