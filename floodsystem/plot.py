# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
plotting.

"""

from floodsystem.datafetcher import fetch_measure_levels
from floodsystem.stationdata import build_station_list
from floodsystem.station import *
from floodsystem.analysis import polyfit
from floodsystem import *

import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import numpy as np

import matplotlib.lines as mlines

def newline(p1, p2):
    ax = plt.gca()
    xmin, xmax = ax.get_xbound()

    if(p2[0] == p1[0]):
        xmin = xmax = p1[0]
        ymin, ymax = ax.get_ybound()
    else:
        ymax = p1[1]+(p2[1]-p1[1])/(p2[0]-p1[0])*(xmax-p1[0])
        ymin = p1[1]+(p2[1]-p1[1])/(p2[0]-p1[0])*(xmin-p1[0])

    l = mlines.Line2D([xmin,xmax], [ymin,ymax])
    ax.add_line(l)
    return l

def plot_water_levels(station, dates, levels):
    
    # Plot
    plt.plot(dates, levels)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    low = station.typical_range[0]
    high = station.typical_range[1]
    #plt.Axes.text(0,low,str(low))
    #plt.text(0,low,"Test")
    #plt.Axes.text(0,high,str(high))
    newline((0,low),(10,low))
    newline((0,high),(10,high))


    plt.show()




def plot_water_level_with_fit(station, dates, levels, p):
    poly, d0,x = polyfit(dates, levels, p)
    #x = plt.dates.date2num(dates)

    # Plot polynomial fit at 30 points along interval (note that polynomial
    # is evaluated using the shift x)
    x1 = np.linspace(x[0], x[-1], d0)
    plt.plot(x1, poly(x1 - x[0]))





    # Plot
    plt.plot(dates, levels)

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels
    low = station.typical_range[0]
    high = station.typical_range[1]
    #plt.Axes.text(0,low,str(low))
    #plt.text(0,low,"Test")
    #plt.Axes.text(0,high,str(high))
    newline((0,low),(10,low))
    newline((0,high),(10,high))
    plt.show()