# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa
from .utils import haversine  # noqa
from floodsystem.stationdata import build_station_list


"""
Sorts the stations based on distance from a given coordinate.
Returns a list of of (station,distance) tuples
"""

def order_by_dist(stations,p):
    big_list = []
    #Uses Haversine function from utils module to compute distances
    for station in stations:
        big_list.append((station.name,haversine(p[0],p[1],station.coord[0],station.coord[1])))
    #Sorts the list by the distance to p
    return sorted_by_key(big_list,1)

"""
Returns a list of stations within a radius of a point
"""
def stations_within_radius(stations, centre, r):
    within_radius = []
    #Iterates over each value to see if it lies in the range
    for station in stations:
        if haversine(centre[0],centre[1],station.coord[0],station.coord[1]) <= r:
            within_radius.append(station.name)
    return within_radius


"""
Given a list of station objects, returns a container with the names of the rivers with a monitoring station
"""
def rivers_with_stations(stations):
    river_list = []
    for station in stations:
        #Tests to see if there is a river associated with the station and not in the list
        if type(station.river)== str and station.river not in river_list:
            river_list.append(station.river)
    return river_list

"""
Makes a dictionary with rivers as the key and a list of stations that are on the river as the value
"""
def stations_by_river(stations):
    river_freq = {}
    for station in stations:
        if station.river in river_freq:
            river_freq[station.river].append(station.name)
        else:
            river_freq[station.river]=[station.name]
    return river_freq


"""def stations_by_river(stations):
    river_list = rivers_with_stations(stations)
    #river_list = sorted_by_key(river_list,1)
    
    #If the key alrready exists then the previous entry is added to otherwise a new key is created
    river_freq = {} 
    for river in river_list:

        if (river[1] in river_freq): 
            river_freq[river[1]].append(river[0])
        else: 
            river_freq[river[1]] = [river[0]]

    return river_freq"""


"""
determines the N rivers with the greatest number of monitoring stations
"""
def rivers_by_station_number(stations, N):
    river_freqs = []
    temp = stations_by_river(stations)

    for item in temp:
        river_freqs.append((item,len(temp[item])))

    river_freqs = sorted_by_key(river_freqs,1,reverse = True)

    #FInds a cutoff such that "ties" are accounted for
    cutoff = river_freqs[N-1][1]
    river_freqs_2 = []
    #Only adds the valid entries to a new list
    for item in river_freqs:
        if item[1] >= cutoff:
            river_freqs_2.append(item)
        else:
            break #save work

    return river_freqs_2

