# Copyright (C) 2018 Lab group 17
#
# SPDX-License-Identifier: MIT

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_within_radius
from floodsystem.geo import order_by_dist


def run():
    """Requirements for Task 1C"""
    print(sorted(stations_within_radius(build_station_list(), (52.2053, 0.1218), 10)))
    


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()
