# Copyright (C) 2018 Lab group 17
#
# SPDX-License-Identifier: MIT

import plotly.graph_objects as go
import pandas as pd
from floodsystem.stationdata import build_station_list	

def run():
    """build a dataframe"""
    lat_lon_name=[]
    for station in build_station_list():
        list_a=[station.coord[0], station.coord[1], station.name]
        lat_lon_name.append(list_a)
    coord_df = pd.DataFrame(lat_lon_name, columns = ['lat' , 'long', 'name']) 

    """locate the stations"""
    fig = go.Figure(data=go.Scattergeo(
            lon = coord_df['long'],
            lat = coord_df['lat'],
            text = coord_df['name'],
            mode = 'markers',
            ))

    """define the scope of the map"""
    fig.update_layout(
            title = 'Locations of the UK Mornitoring Stations',
            geo=dict(
                showrivers = True,
                rivercolor = "navy",
                showcountries = True,
                countrycolor = "skyblue",
                lataxis=dict(
                    range=[50,56]
                ),
                lonaxis=dict(
                    range=[-8,2]
                ),
                projection = dict(
                    type = 'natural earth'
                ),
            ),
        )
    fig.show()

if __name__ == "__main__":
    print("*** Extension Task: CUED Part IA Flood Warning System ***")
    run()