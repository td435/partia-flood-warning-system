# Copyright (C) 2018 Lab group 17
#
# SPDX-License-Identifier: MIT

from floodsystem.stationdata import build_station_list
from floodsystem.station import inconsistent_typical_range_stations


def run():
    corrupt = inconsistent_typical_range_stations(build_station_list())
    print(sorted(corrupt))


    


if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()
