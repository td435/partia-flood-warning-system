from floodsystem.datafetcher import fetch, fetch_measure_levels
from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.warninglevel import warning_level, warning_level1


def run():

    stations = build_station_list()
    update_water_levels(stations)

    for station in stations_highest_rel_level(stations, 10):
        print (station.name + ": " + warning_level(station))

    m=[]
    for station in stations_highest_rel_level(stations, 30):
        if warning_level(station)=="high":
            m.append(station.name)
    print(m)

    n=[]
    for station in stations:
        if station.name[0]=="G":
            if warning_level(station)=="low":
                n.append(station.name)
    print(n)

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()
