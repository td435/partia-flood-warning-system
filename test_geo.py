# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""Unit test for the Geo module"""

from floodsystem.geo import *
from floodsystem.stationdata import build_station_list

"""
Tests that the function can be called and a list is returned
"""
def test_order_by_dist():
    assert type(order_by_dist(build_station_list(),(52.2053, 0.1218))) == list

    #test that the lists are arranged according to the distance
    a=order_by_dist(build_station_list(),(52.4120, -0.1127))
    for i in range(100, 120):
        assert a[i][1]<a[i+1][1]

"""
Tests that a list can be returned without crashing
"""
def test_range_func():
    assert type(stations_within_radius(build_station_list(), (52.2053, 0.1218), 15)) == list


"""
Tests that the function can be called and a list is returned
"""
def test_river_with_stations():
    assert type(rivers_with_stations(build_station_list())) == list
    
    #no duplicated river
    a=rivers_with_stations(build_station_list())
    a.sort()
    for i in range(50, 70):
        assert a[i]!=a[i+1]

"""
Tests that all rivers in the dictionary have at least one entry
"""
def test_stations_by_river():
    test_dict = stations_by_river(build_station_list())
    for key in test_dict:
        assert len(test_dict[key])>0

"""
Tests that the list is not shorter than it should be and that all the lowest values are the same
"""
def test_rivers_by_station_number():
    rivers = rivers_by_station_number(build_station_list(), 10)
    assert len(rivers) >= 10
    cutoff = rivers[9][1]
    for item in rivers[10:len(rivers)]:
        assert item[1] == cutoff