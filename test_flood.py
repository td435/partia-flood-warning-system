from floodsystem.flood import *
from floodsystem.stationdata import build_station_list, update_water_levels

def test_stations_level_over_threshold():
    stations=build_station_list()
    update_water_levels(stations)
    #test a list of tuple is returned
    assert type(stations_level_over_threshold(stations, 0.5))==list
    assert type(stations_level_over_threshold(stations, 0.5)[1])==tuple

    #test the level is over threshold
    assert stations_level_over_threshold(stations, 0.5)[-1][1]>0.5

def test_stations_highest_rel_level():
    stations=build_station_list()
    update_water_levels(stations)
    #test a list of tuple is returned
    assert type(stations_highest_rel_level(stations, 20))==list
    #assert type(stations_highest_rel_level(stations, 5)[1])==tuple

    #This is wrong
    assert stations_highest_rel_level(stations, 20)[4].relative_water_level()>\
        stations_highest_rel_level(stations, 20)[16].relative_water_level()