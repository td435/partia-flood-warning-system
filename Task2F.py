# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

import matplotlib

import datetime

from datetime import datetime, timedelta

from floodsystem.datafetcher import fetch_measure_levels

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import *
from floodsystem.flood import *
from floodsystem.station import *
from floodsystem.analysis import *
from floodsystem import *



def run():

    # Build list of stations
    stations = build_station_list()
    update_water_levels(stations)
    dt = 2
    for station in stations_highest_rel_level(stations, 5):
        dates, levels = fetch_measure_levels(station.measure_id, dt=timedelta(days=dt))
        plot_water_level_with_fit(station, dates, levels, 4)

    

   


if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()
