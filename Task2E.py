# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT

from datetime import datetime, timedelta

from floodsystem.datafetcher import fetch_measure_levels

from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.plot import *
from floodsystem.flood import *
from floodsystem.station import *

def run():

    # Build list of stations
    stations = build_station_list()
    update_water_levels(stations)
    for station in stations_highest_rel_level(stations, 5):
        
        #station = item[0]
        #print(type(station.typical_range[1]))
        dt = 10
        dates, levels = fetch_measure_levels(station.measure_id, dt=timedelta(days=dt))
        plot_water_levels(station, dates, levels)
        
   


if __name__ == "__main__":
    print("*** Task 2E: CUED Part IA Flood Warning System ***")
    run()
